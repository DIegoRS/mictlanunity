﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager Instance = null; 
    public PlayerController lp_guy;
    public PlayerController nina;
    public GameObject select01;
    public GameObject select02; 
    public PlayerController Player;                       
     void FixedUpdate()
    {
        select01= GameObject.FindGameObjectWithTag("Player");
        select02= GameObject.FindGameObjectWithTag("select02");


        if (select01==true){
                Player=lp_guy;
        }
         if (select02==true){
                Player=nina;
        }
    }
    void Awake()
    {

        if (Instance == null)
        {
            Instance = this;
        }

        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        // Dont destroy on reloading the scene
        // DontDestroyOnLoad(gameObject);

 
    }

    
    
}
