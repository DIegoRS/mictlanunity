﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dontdestroymusica : MonoBehaviour
{
    // Start is called before the first frame update
    AudioSource funeteaudio;
    public AudioClip musicapirncipal;
    private void Awake() {
        DontDestroyOnLoad(this.gameObject);// este es un obejto de tipo musica
        // le decimos que el obejto musica no se destruya cuando pasemos de una escena a otra
        // asi mantendremos la misma musica a lo largo del juego
    }
    public void destruirmusica() {
         
         Destroy(GameObject.Find("musicamenu")); 
         // si hay un objeto de tipo musicamenu lo destruira cuando se instancie este obejeto
    }
    void Start()
    {
        funeteaudio= GetComponent<AudioSource>();
        funeteaudio.clip= musicapirncipal;// en el metodo clip igualamos el clip(la musica) que queremos activar
        if(!funeteaudio.isPlaying){
             funeteaudio.Play();// finalmente damos play al clip
        }
              
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
