﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class transiciones : MonoBehaviour
{
    private Animator _transicionAnim;
    // Start is called before the first frame update
    void Start()
    {
        _transicionAnim= GetComponent<Animator>();
    }
     public void LoadScene(string scene){
              StartCoroutine(Transicionada(scene));
             
     }
    // Update is called once per frame
   IEnumerator Transicionada(string scene){
        _transicionAnim.SetTrigger("salida");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(scene);
    }
    void Destruction(){
              Destroy(this.gameObject);   
    }
}
 