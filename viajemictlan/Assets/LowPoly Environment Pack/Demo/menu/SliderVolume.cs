﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderVolume : MonoBehaviour
{
    // Start is called before the first frame update
    public Slider slidermusica;
    public  AudioSource musica;
    private void Awake() {
        actuaizarslider();
    }
    void Start()
    {
        slidermusica=this.gameObject.GetComponent<Slider>();
        //musica=GameObject.FindGameObjectWithTag("musica2").GetComponent<AudioSource>();
        slidermusica.value=musica.volume;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void VolumenMusicaUpadte(){
          musica.volume=slidermusica.value;
          PlayerPrefs.SetFloat("musicavolum", musica.volume);
          PlayerPrefs.Save();
    }
    private void actuaizarslider(){
        musica.volume=PlayerPrefs.GetFloat("musicavolum",1.0f);

    }
}
