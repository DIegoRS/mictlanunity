﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarcoController2 : MonoBehaviour
{
    private float horizontalMove;
    private float verticalMove;
    private Vector3 playerInput;
    public CharacterController player;
    public float playerSpeed;
    private Vector3 movePlayer;
    public float gravity = 10.0f;
    public Camera mainCamera;
    private Vector3 camForward;
    private Vector3 camRight;
    public float fallVelocity;
    public float jumpForce;
    private GameObject select01;
    private GameObject select02;
   
    // Start is called before the first frame update
    void Start()
    {   
        player = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {      //en este sector podenmos mover el barco
      
            horizontalMove = Input.GetAxis("Horizontal");// usamos el metodo getAxists para dectetra las corednadas 
            //para movemos en el eje X
            //aqui nos movemos en el eje z
            verticalMove = +15;

            playerInput = new Vector3(horizontalMove * 10, 0, verticalMove);//en este hacemos el caulod ela transofrmacion 
            //aumentamos en z para ir hacia adelante
            playerInput = Vector3.ClampMagnitude(playerInput, 1); 

            camDirection();// llamamos la metodo del movimito de la camara para segir al personaje

            movePlayer = playerInput.x * camRight + playerInput.z * camForward;// hacemoso la operacion para opbtener la
            // velocidad de la camar y de nuetsro personaje
            movePlayer = movePlayer * playerSpeed;

            setGravity();
            playerSkills();
            player.Move(movePlayer * Time.deltaTime);// hacemos la transfromacion del personaje
     
        
    }
    void camDirection()
    {
        camForward = mainCamera.transform.forward;
        camRight = mainCamera.transform.right;

        camForward.y = 0;
        camRight.y = 0;

        camForward = camForward.normalized;
        camRight = camRight.normalized;

    }
    void setGravity()
    {

        if (player.isGrounded)
        {
            fallVelocity = -gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;
        }
        else
        {
            fallVelocity -= gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;
        }

    }
    void playerSkills()
    {
        if (player.isGrounded && Input.GetButtonDown("Jump"))
        {

            fallVelocity = jumpForce;
            movePlayer.y = fallVelocity;


        }

    }
}
