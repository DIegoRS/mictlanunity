﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorial : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject canvastutorial;
    public GameObject canvasHUD;
    public GameObject canvasDialogo;
    void Start()
    {
         Time.timeScale=0f;// paramos el juego cuando el canvas de los controles este activado
    }

    // Update is called once per frame
    void Update()
    {
        
    }
   public void entendido(){
         Time.timeScale=1f;//activamos otra vez el juego
         canvastutorial.SetActive(false);//ocultamos el canvas del los controles
         canvasHUD.SetActive(true);// activamos el canvas que vamos a usar en el nivel
         canvasDialogo.SetActive(true);
        
        
    }
    
}
