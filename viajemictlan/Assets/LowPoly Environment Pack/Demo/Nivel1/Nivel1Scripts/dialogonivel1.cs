﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class dialogonivel1 : MonoBehaviour
{
    // Start is called before the first frame update
    public  dialog dialogo;
    Queue<string> sentences;
    public GameObject dialogopanel;
    public TextMeshProUGUI displayText;
    string sentenciactiva;
    public float speed;
    AudioSource myaudio;
    public AudioClip speak;
    int conversacion =0;
    bool comienzodedialogo= true;

    void Start()
    {
        sentences= new Queue<string>();
    }

    // Update is called once per frame
    void Update()
    {
         if(comienzodedialogo){
            dialogopanel.SetActive(true);
            StartDialogo();
            comienzodedialogo=false;

         }
       
          
        
         if(Input.GetKey(KeyCode.Return)){
            dipslaynextSentence();
         }
    }
    IEnumerator TypeTheSentence(string sentence){
         displayText.text="";
         foreach(char letter in sentence.ToCharArray()){
                displayText.text += letter.ToString();
                yield return new WaitForSeconds(speed);
                
         }
    }
     public void StartDialogo(){
        sentences.Clear();
        foreach (string sentence in  dialogo.enunciadoslist)
        {
            sentences.Enqueue(sentence);// este for cha recorre los dialogos que estan almecenados en un array
            


        }
        dipslaynextSentence();
    }
    private void dipslaynextSentence(){
               if(sentences.Count<=0){
                   displayText.text=sentenciactiva;
                   return;
               }
               sentenciactiva=sentences.Dequeue();//esta variable almacena el dialogo de se guardo en sentences
               displayText.text=sentenciactiva;// al final manda el enucniao a nuestro crudro de texto de la escena
               StopAllCoroutines();
               StartCoroutine(TypeTheSentence(sentenciactiva));
    }
    private void OnMouseDown() {
       
        dialogopanel.SetActive(true);
           if(conversacion>0){// en este caso activaremos los dialogos cuando seleccionemso a nuestro perro.
            
            dipslaynextSentence();
           }else{
              
               StartDialogo();
           }
        conversacion++;
    }
   
}
