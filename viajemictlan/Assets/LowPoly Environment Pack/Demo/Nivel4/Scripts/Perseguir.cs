﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perseguir : MonoBehaviour
{
    // Start is called before the first frame update
    public float movementSpeed=1f;// declaramos la velocidad de la flecha se puede configurarde desde unity
    public int elevacion;// esta variable va a pocisonar el objeto de las flechas en un punto del eje y
    Vector3 targetPosition;// los vector3 nos dara la posicion de nuestro objeto flecha y permite manipularla
    Vector3 towardsTarget;//
    float wanderRadius = 4f;// 4f sginifca le tamaño del radio dennsytra flecha respecto a nutro personaje
    void RecalculateTargetPosition()
    {
        targetPosition = transform.position + Random.insideUnitSphere * wanderRadius;// calculamos el radio,
        // la posicion de utsra flecha, mas un numero random dentro del mapa para que le signe una direccion
        // para moverese en el eje z y x
        targetPosition.y = elevacion;// finalmente le dcimo sobre que eje queremos que semueva con el valor de 
        //4f sobre le eje Y
    }
    void Start()
    {
        RecalculateTargetPosition();//llama al metodo reculcular posicion
    }

    // Update is called once per frame
    void Update()
    {
        towardsTarget = targetPosition - transform.position;//
        if (towardsTarget.magnitude < 0.50f)
            RecalculateTargetPosition();
        Quaternion towardsTargetRotation = Quaternion.LookRotation(towardsTarget, Vector3.up);// le asignamos los 
        // puntos de la rotacion a nustra flecha para hacer el giro o cambio de direccion
        transform.LookAt(targetPosition);
        transform.position += towardsTarget.normalized * movementSpeed * Time.deltaTime;// volvemo areculcular la pocision
        Debug.DrawLine(transform.position, targetPosition, Color.green);
    }
}
