﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spwanerrocasDemo2 : MonoBehaviour
{
    // Start is called before the first frame update
    
    public GameObject spawnroca;
    public bool stopSpawning=false;
    public float spawTime;
    public float spawdelay;


    void Start(){
       
        InvokeRepeating("SpawnObject",1,15); //este metodo invoca a otra metodo cada cierto tiempo y cierta
        //canatidad e veces
    }


    void SpawnObject(){// en este metodo decimos qu etiene que spawnear una roca y le pasamos las cordenas donde
    // tendran que apracer
        float z= Random.Range(-30.0f,30.0f);
        float x= Random.Range(-11.0f,-9.0f);
        
        Instantiate(spawnroca,new Vector3(-9,16,z),Quaternion.identity);// instacnciamos el objeto roca
        if(stopSpawning){
                CancelInvoke("SpawnObject");
        }
    }
  
}
