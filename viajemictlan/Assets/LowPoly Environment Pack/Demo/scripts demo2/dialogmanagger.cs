﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class dialogmanagger 
{
    // Start is called before the first frame update
    public  dialog dialogo;
    Queue<string> sentences;
    public GameObject dialogopanel;
    public TextMeshProUGUI displayText;
    string sentenciactiva;
    public float speed;
    AudioSource myaudio;
    public AudioClip speak;
     void Start()
    {
         sentences= new Queue<string>();
  

    }
    public void StartDialogo(){
        sentences.Clear();
        foreach (string sentence in  dialogo.enunciadoslist)
        {
            sentences.Enqueue(sentence);


        }
        dipslaynextSentence();
    }
    private void dipslaynextSentence(){
               if(sentences.Count<=0){
                   displayText.text=sentenciactiva;
                   return;
               }
               sentenciactiva=sentences.Dequeue();
               Debug.Log(sentenciactiva);
    }
    
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
