﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pausaActive : MonoBehaviour
{
    // Start is called before the first frame update
    bool active;
    public GameObject pause;
    void Start()
    {
     
       
    }

    // Update is called once per frame
   
    public void activarPause(){
            Time.timeScale=0f;// para deplegar el menu de pausa primero pararemos al tiempo en le juego
            pause.SetActive(true);// activaremos el menu de pausa
    }
     public void desactivarPause(){
            Time.timeScale=1f;// cuando presionamos en el boton resume volveremos a activar el tiempo del juego
            pause.SetActive(false);// desactivaremos el menu de pausa
    }
}
