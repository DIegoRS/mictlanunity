﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptcamara : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject select01;
    public GameObject select02;
     public GameObject lp_guy;
    public GameObject nina;
    public GameObject player;
    public Vector3 distancia;
    void Start()
    {   
        select01= GameObject.FindGameObjectWithTag("Player");
        select02= GameObject.FindGameObjectWithTag("select02");
        
        if(select01==true){
        player=lp_guy;
        }
        if(select02==true){
         player=nina;
        }
        distancia= transform.position-player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position=player.transform.position + distancia;
    }
}
