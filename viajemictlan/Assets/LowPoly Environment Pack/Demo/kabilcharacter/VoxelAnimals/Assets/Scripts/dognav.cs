﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
public class dognav : MonoBehaviour
{

    private float movementSpeed = 2.7f;
    private float jumpForce = 300;
    private float timeBeforeNextJump = 1.2f;
    private float canJump = 0f;
    public float distance = 3.0f;
    public GameObject select01;
    public GameObject select02;
    public GameObject lp_guy;
    public GameObject nina;
    public GameObject player;
    public NavMeshAgent nav;
    Animator anim;
    Rigidbody rb;
    


    public  dialog dialogo;
    Queue<string> sentences;
    public GameObject dialogopanel;
    public TextMeshProUGUI displayText;
    string sentenciactiva;
    public float speed;
    AudioSource myaudio;
    public AudioClip speak;
    int conversacion =0;

  

    void Start()

    {   

        select01= GameObject.FindGameObjectWithTag("Player");//seleciona que perosonaje esta visble en la escena
        select02= GameObject.FindGameObjectWithTag("select02");
        
        if(select01==true){
          player=lp_guy;
        }
        if(select02==true){
          player=nina;
        }
        
         nav=GetComponent <NavMeshAgent>();//navmesh es una librria de unity que plaica Ai a nuestro objeto
         anim = GetComponent<Animator>();
         rb = GetComponent<Rigidbody>();
         sentences= new Queue<string>();
    }
    
    void Update()
    {
        //ControllPlayer();
       
        moverpersonaje();
    }
    void moverpersonaje(){
       if(Vector3.Distance(player.transform.position,transform.position)< distance){
           nav.SetDestination(player.transform.position);//le dceimos a nuetsro perro wue siga a nuetsro jugador
           anim.SetInteger("Walk",1);// activamos la animacion
           nav.speed=3;
          
       }else{
           
           nav.speed=0;
           anim.SetInteger("Walk",0);
       }
       if(Input.GetKey(KeyCode.Return)){

            dialogopanel.SetActive(false);
            
       }
          
    }
    void ControllPlayer()
    {
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        if (movement != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movement), 0.15f);
            anim.SetInteger("Walk", 1);
        }
        else {
            anim.SetInteger("Walk", 0);
        }

        transform.Translate(movement * movementSpeed * Time.deltaTime, Space.World);

        if (Input.GetButtonDown("Jump") && Time.time > canJump)
        {
                rb.AddForce(0, jumpForce, 0);
                canJump = Time.time + timeBeforeNextJump;
                anim.SetTrigger("jump");
        }
    }
      IEnumerator TypeTheSentence(string sentence){
         displayText.text="";
         foreach(char letter in sentence.ToCharArray()){
                displayText.text += letter.ToString();
                yield return new WaitForSeconds(speed);
                
         }
    }
    public void StartDialogo(){
        sentences.Clear();
        foreach (string sentence in  dialogo.enunciadoslist)
        {
            sentences.Enqueue(sentence);// este for cha recorre los dialogos que estan almecenados en un array
            


        }
        dipslaynextSentence();
    }
    private void dipslaynextSentence(){
               if(sentences.Count<=0){
                   displayText.text=sentenciactiva;
                   return;
               }
               sentenciactiva=sentences.Dequeue();//esta variable almacena el dialogo de se guardo en sentences
               displayText.text=sentenciactiva;// al final manda el enucniao a nuestro crudro de texto de la escena
               StopAllCoroutines();
               StartCoroutine(TypeTheSentence(sentenciactiva));
    }
    private void OnMouseDown() {
       
        dialogopanel.SetActive(true);
           if(conversacion>0){// en este caso activaremos los dialogos cuando seleccionemso a nuestro perro.
            
            dipslaynextSentence();
           }else{
              
               StartDialogo();
           }
        conversacion++;
    }
   
    

}