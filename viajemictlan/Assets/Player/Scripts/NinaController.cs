﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinaController : MonoBehaviour
{
    private float horizontalMove;
    private float verticalMove;
    private Vector3 playerInput;
    public CharacterController player;
    public float playerSpeed;
    private Vector3 movePlayer;
    public float gravity = 10.0f;

    private Animator anim;
    public Camera mainCamera;
    private Vector3 camForward;
    private Vector3 camRight;
    public float fallVelocity;
    public float jumpForce;
    private int Health=100;


    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxis("Horizontal");
        verticalMove = Input.GetAxis("Vertical");
        
        playerInput = new Vector3(horizontalMove, 0, verticalMove);
        playerInput = Vector3.ClampMagnitude(playerInput, 1);
        anim.SetFloat("Caminar",playerInput.magnitude*playerSpeed);
        camDirection();

        movePlayer = playerInput.x * camRight + playerInput.z * camForward;
        movePlayer = movePlayer * playerSpeed;
        player.transform.LookAt(player.transform.position + movePlayer);
        setGravity();
        playerSkills();
        player.Move(movePlayer * Time.deltaTime);
     

    }
    void camDirection()
    {
        camForward = mainCamera.transform.forward;
        camRight = mainCamera.transform.right;

        camForward.y = 0;
        camRight.y = 0;

        camForward = camForward.normalized;
        camRight = camRight.normalized;

    }
    void setGravity()
    {

        if (player.isGrounded)
        {
            fallVelocity = -gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;
        }
        else
        {
            fallVelocity -= gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;
        }
        anim.SetBool("Suelo", player.isGrounded);
    }
    void playerSkills()
    {
        if (player.isGrounded && Input.GetButtonDown("Jump"))
        {
            anim.SetTrigger("Salta");
            fallVelocity = jumpForce;
            movePlayer.y = fallVelocity;            
        }

    }
     public bool TakeDamage(int amount)
    {
        Health -= amount;
        if (Health <=0){
              return true;
        } else{
            return false;     
        }
            
    }
    
}
