﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawnerflech : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject spawnflech;
    public bool stopSpawning = false;
    public float spawTime;
    public float spawdelay;
    void Start()
    {
        InvokeRepeating("SpawnObject", 1, 15);
    }
    void SpawnObject()
    {
        float z = Random.Range(-10.0f, 0.0f);
        float x = Random.Range(-10.0f, 0.0f);

        Instantiate(spawnflech, new Vector3(-9, 16, z), Quaternion.identity);
        if (stopSpawning)
        {
            CancelInvoke("SpawnObject");
        }
    }
}
